= Change Log

All noteable changes to this project will be documented in this file.
This project adheres to http://semver.org/[Semantic Versioning].

== [1.3.0] – 2022-10-29

* fix: failing pytest – add testinfra requirement

== [1.2.0] – 2022-09-25

* fix: add pytest requirement, necessary for some molecule tests

== [1.1.0] – 2022-09-25

* feat: use prebuilt docker image

== [1.0.2] – 2022-09-25

* fix: python wheel now needs rust toolchain

== [1.0.1] – 2022-09-25

* [.line-through]#fix: python wheel now needs rust toolchain#

== [1.0.0] – 2022-01-30

* feat: make molecule independent of podman or docker, we use what is present

== [0.2.0] – 2022-01-30

* feat: add central link:templates/requirements.txt[requirements.txt], and use it for `pip install` as part of link:templates/molecule-role.gitlab-ci.yml[our .gitlab-ci.yml template]

== [0.1.0] – 2022-01-23

* Initial release
